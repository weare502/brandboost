<?php get_header(); ?>
<div class="apply vote-page">
	<h1 class="centered"><span><?php the_field('top_title'); ?></span></h1>
	<div class="container give-padding top pre-launch">
		<h1 class="logo"><a href="/" title="Learn more about Brand Boost!"><span class="screen-reader-text">Brand Boost</span></a></h1>
		<div class="countdown">
			<h1 class="centered"><?php the_field('header_text'); ?></h1>
			<div class="clear"><?php the_field('header_long_text'); ?></div>
			<?php echo do_shortcode( '[ujicountdown id="Black" expire="' . get_field('vote_time_over') . '" hide="true" url="http://brandboost.org/" rectype="second"]' ); ?>
			<div class="banner">Until Voting is Closed</div>
		</div>
	</div>

	<div class="black-bg mission clear">

		<div class="container give-padding the-content">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; endif; ?>

		</div>

	</div>

	<div class="black-bg mission clear">

		<div class="container give-padding">

			<?php $rows = get_field('organizations'); shuffle($rows); ?>

				<div class="clear">

				<?php foreach ( $rows as $row ) : ?>

					<div class="organization">

						<img src="<?php echo $row['image']['sizes']['large']; ?>" />

						<h3 class="org-name"><?php echo $row['name']; ?></h3>

						<p class="small">
							<?php echo $row['description']; ?>
						</p>

						<a href="#vote-form" class="vote button popup-with-form">Vote</a>

						<input type="hidden" class="fb-url" value="<?php echo $row['fb_link']; ?>" />

					</div>

				<?php endforeach; ?>

				</div>

			
		</div>

	</div>

	<footer class="clear">

		<div class="container give-padding">
		
			<div class="third">

				<h2>#BrandBoost2015</h2>

				<div class="container give-padding centered share">
					<h4 class="header">Share Brand Boost</h4>
					<div class="buttons">
						<a href="https://www.facebook.com/sharer/sharer.php?u=brandboost.org/vote"><i class="fa fa-facebook"></i></a>
						<a href="https://twitter.com/home?status=%23BrandBoost2015%20is%20here%20and%20donating%20$25,000%20to%20a%20local%20Manhattan%20business!%20http%3A//brandboost.org/"><i class="fa fa-twitter"></i></a>
						<a href="https://instagram.com/explore/tags/brandboost2015/"><i class="fa fa-instagram"></i></a>
					</div>
				</div>

			</div>

			<div class="third">

				<iframe width="560" height="315" src="<?php the_field( 'video_embed_url', 6 ); ?>" frameborder="0" allowfullscreen></iframe>

			</div>

			<div class="third">
				<img class="our-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/502-logo.png" />
				<?php the_field('502_bio', 'option'); ?>
			</div>

		</div>

	</footer>

</div>

<div id="vote-form" class="white-popup-block mfp-hide">
	<h1>You are voting for <br><span id="voting-for"></span></h1>
	<?php echo do_shortcode( '[gravityform id="2" title="false" description="false" ajax="true"]' ); ?>
	<div class="container give-padding centered share">
		<h4 class="header">Share Who You Voted For!</h4>
		<div class="buttons">
			<a class="fb" href="https://www.facebook.com/sharer/sharer.php?u=[placeholder]" data-original="https://www.facebook.com/sharer/sharer.php?u=[placeholder]" target="_blank"><i class="fa fa-facebook"></i></a>
			<a class="twitter" href="https://twitter.com/home?status=%23BrandBoost2015+I+voted+for+[placeholder]+%0ACast+your+vote+now%20http%3A//brandboost.org/vote" data-original="https://twitter.com/home?status=%23BrandBoost2015+I+voted+for+[placeholder]+%0A+Cast+your+vote+now%20http%3A//brandboost.org/vote" target="_blank"><i class="fa fa-twitter"></i></a>
		</div>
	</div>
</div>


<?php get_footer(); ?>
