<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Brand Boost
 */

get_header(); ?>

	<?php if ( $_GET['502-preview'] === 'true' ||  current_time( 'timestamp' ) > strtotime('2015/09/15 12:00') ) : ?>
		
		<div class="launched">
			
			<div class="container give-padding top">
				<h1 class="logo"><span class="screen-reader-text">Brand Boost</span></h1>
				<h2 class="tagline centered">Does your brand need a boost?</h2>
				<p class="centered">Applications are in! Which organization do you want to see boosted?</p>
			</div>

			<a href="/vote" class="apply-now move-down"><span class="button">Vote Now!</span></a>

			<div class="blue-bg clear mission">

				<div class="container give-padding">
				
					<h2 class="centered move-down"><span class="underline"><?php the_field('header1'); ?></span></h2>
					<?php the_field('paragraph1'); ?>

					<hr class="stars">

					<h2 class="centered"><span class="underline"><?php the_field('header2'); ?></span></h2>

					<?php the_field('paragraph2'); ?>

					<div class="pull-image-left image-1">

						<h3 class="title">
							<span class="number">01</span> <?php the_field('list1_title'); ?>
						</h3>
						<?php the_field('list1_paragraph'); ?>

					</div>

					<div class="pull-image-left image-2">

						<h3 class="title">
							<span class="number">02</span> <?php the_field('list2_title'); ?>
						</h3>
						<?php the_field('list2_paragraph'); ?>

					</div>
					
					<div class="pull-image-left image-3">

						<h3 class="title">
							<span class="number">03</span> <?php the_field('list3_title'); ?>
						</h3>
						<?php the_field('list3_paragraph'); ?>

					</div>
					
					<div class="pull-image-left image-4">

						<h3 class="title">
							<span class="number">04</span> <?php the_field('list4_title'); ?>
						</h3>
						<?php the_field('list4_paragraph'); ?>

					</div>

				</div>

			</div>

			<div id="video" class="pink-bg clear">

				<div class="container give-padding">
				
					<div class="half">
						<iframe width="560" height="315" src="<?php the_field( 'video_embed_url' ); ?>" frameborder="0" allowfullscreen></iframe>
					</div>

					<div class="half">
						
						<h2 class="what-it-takes centered"><?php the_field('header3'); ?></h2>

						<?php the_field('paragraph3'); ?>
						
						<p class="centered"><a href="/vote" class="apply"><span class="button">Vote Now</span></a></p>

					</div>

				</div>

			</div>

			<footer class="clear">

				<div class="container give-padding">
				
				<div class="third">

					<h2>#BrandBoost2015</h2>

					<div class="container give-padding centered share">
						<h4 class="header">Share Brand Boost</h4>
						<div class="buttons">
							<a href="https://www.facebook.com/sharer/sharer.php?u=brandboost.org"><i class="fa fa-facebook"></i></a>
							<a href="https://twitter.com/home?status=%23BrandBoost2015%20is%20here%20and%20donating%20$25,000%20to%20a%20local%20Manhattan%20business!%20http%3A//brandboost.org/"><i class="fa fa-twitter"></i></a>
							<a href="https://instagram.com/explore/tags/brandboost2015/"><i class="fa fa-instagram"></i></a>
						</div>
					</div>

				</div>

				<div class="third">
					<div class="banner">Voting Ends In</div>
					<div class="countdown">
						<?php echo do_shortcode( '[ujicountdown id="Black" expire="2015/10/10 00:00" hide="true" url="http://brandboost.org/" rectype="second"]' ); ?>
					</div>

				</div>
				<div class="third">
					<img class="our-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/502-logo.png" />
					<?php the_field('502_bio', 'option'); ?>
				</div>

				</div>

			</footer>

		</div>

	<?php else : ?>

		<div class="container give-padding top pre-launch">
			<h1 class="logo"><span class="screen-reader-text">Brand Boost</span></h1>
			<div class="countdown">
				<?php echo do_shortcode( '[ujicountdown id="Black" expire="2015/09/15 12:00" hide="true" url="http://brandboost.org/" rectype="second"]' ); ?>
				<div class="banner">Until Mission Initiation</div>
				<p>502 Media Group is excited to present: Brand Boost - 
				the biggest marketing give back campaign in Manhattan. See the countdown clock for 
				official launch of this event and check back here for all the exciting details to come. 
				Follow <a href="https://twitter.com/hashtag/brandboost2015">#brandboost2015</a> and the 
				<a href="https://www.facebook.com/502MediaGroup/">502 Facebook page</a> for real time updates.</p>
			</div>
		</div>

		<div class="container give-padding centered share">
			<h4 class="header">Share Brand Boost</h4>
			<div class="buttons">
				<a href="https://www.facebook.com/sharer/sharer.php?u=brandboost.org"><i class="fa fa-facebook"></i></a>
				<a href="https://twitter.com/home?status=%23BrandBoost2015%20is%20here%20and%20donating%20$25,000%20to%20a%20local%20Manhattan%20business!%20http%3A//brandboost.org/"><i class="fa fa-twitter"></i></a>
				<a href="https://instagram.com/explore/tags/brandboost2015/"><i class="fa fa-instagram"></i></a>
			</div>
		</div>

	<?php endif; ?>


<?php get_footer(); ?>
