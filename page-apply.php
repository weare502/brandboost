<?php get_header(); ?>
<div class="apply">
	<h1 class="centered"><span><?php the_field('top_title'); ?></span></h1>
	<div class="container give-padding top pre-launch">
		<h1 class="logo"><span class="screen-reader-text">Brand Boost</span></h1>
		<div class="countdown">
			<?php echo do_shortcode( '[ujicountdown id="Black" expire="' . get_field('time_over') . '" hide="true" url="http://brandboost.org/" rectype="second"]' ); ?>
			<div class="banner">Until Applications are Due</div>
			<!-- <p>502 Media Group is excited to present: Brand Boost - the biggest marketing give back campaign in Manhattan. See the countdown clock for official launch of this event and check back here for all the exciting details to come. Follow <a href="https://twitter.com/hashtag/brandboost2015">#brandboost2015</a> and the <a href="https://www.facebook.com/502MediaGroup/">502 Facebook page</a> for real time updates.</p> -->
		</div>
	</div>
	<p class="small centered"><?php the_field('top'); ?></p>

	<div class="black-bg mission clear">

		<div class="container give-padding">
			
			<h2 class="centered"><?php the_field('heading1'); ?></h2>
			
				<?php the_field('paragraph1'); ?>
			

			<h2 class="centered"><?php the_field('heading2'); ?></h2>
			
				<?php the_field('paragraph2'); ?>
			
		</div>

	</div>

	<div class="pink-bg clear">

	<div class="container give-padding">

		<h2 class="centered"><?php the_field('heading3'); ?></h2>
		
		
			<?php the_field('paragraph3'); ?>

			<?php echo do_shortcode( get_field('gravity_form_shortcode' ) ); ?>
		
		
	</div>

	</div>

	<div class="container give-padding centered winners clear">

		<hr class="stars">
		
		<h2><?php the_field('winners_heading'); ?></h2>

		<hr class="stars">

	</div>

	<footer class="clear">

		<div class="container give-padding">
		
		<div class="third">

			<h2>#BrandBoost2015</h2>

			<div class="container give-padding centered share">
				<h4 class="header">Share Brand Boost</h4>
				<div class="buttons">
					<a href="https://www.facebook.com/sharer/sharer.php?u=brandboost.org"><i class="fa fa-facebook"></i></a>
					<a href="https://twitter.com/home?status=%23BrandBoost2015%20is%20here%20and%20donating%20$25,000%20to%20a%20local%20Manhattan%20business!%20http%3A//brandboost.org/"><i class="fa fa-twitter"></i></a>
					<a href="https://instagram.com/explore/tags/brandboost2015/"><i class="fa fa-instagram"></i></a>
				</div>
			</div>

		</div>

		<div class="third">
			<!-- <div class="banner">Application for Mission Due In</div> -->
			<div class="countdown">
				<?php//  echo do_shortcode( '[ujicountdown id="Black2" expire="2015/09/15 12:00" hide="true" url="http://brandboost.org/" rectype="second"]' ); ?>
			</div>

		</div>
		<div class="third">
			<img class="our-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/502-logo.png" />
			<?php the_field('502_bio', 'option'); ?>
		</div>

		</div>

	</footer>

</div>

<?php get_footer(); ?>
