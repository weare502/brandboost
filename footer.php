<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Brand Boost
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info centered">
			<p>Made with <i class="fa fa-heart"></i> by <a href="http://502mediagroup.com/">502 Media Group</a> in Manhattan, KS</p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript" src="
https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.1.0/jquery.fitvids.js"></script>
<script type="text/javascript">
	jQuery(document).ready( function(){
		jQuery('#page').fitVids();
	});
</script>
 <script src="https://use.typekit.net/beq4lao.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
</body>
</html>
