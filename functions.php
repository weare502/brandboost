<?php
/**
 * Brand Boost functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package Brand Boost
 */

if ( ! function_exists( 'brandboost_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function brandboost_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Brand Boost, use a find and replace
	 * to change 'brandboost' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'brandboost', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'brandboost' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'brandboost_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // brandboost_setup
add_action( 'after_setup_theme', 'brandboost_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function brandboost_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'brandboost_content_width', 640 );
}
add_action( 'after_setup_theme', 'brandboost_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function brandboost_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'brandboost' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'brandboost_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function brandboost_scripts() {
	wp_enqueue_style( 'brandboost-style', get_stylesheet_uri(), array(), '20150923' );

	wp_enqueue_style( 'brandboost-magnific-popup-css', get_template_directory_uri() . '/bower_components/magnific-popup/dist/magnific-popup.css', array(), '20120208');

	wp_enqueue_script( 'brandboost-magnific-popup', get_template_directory_uri() . '/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js', array( 'jquery' ), '20120208', true );

	wp_enqueue_script( 'brandboost-navigation', get_template_directory_uri() . '/js/navigation.js', array( 'jquery', 'brandboost-magnific-popup' ), '20120208', true );

	wp_enqueue_script( 'brandboost-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'brandboost_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'SETTINGS ENTER THEM HERE',
		'menu_title'	=> 'SETTINGS ENTER THEM HERE',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}

function brand_boost_add_dashboard_widgets() {

	wp_add_dashboard_widget(
                 'brand_boost_dashboard_widget',         // Widget slug.
                 'Brand Boost Standings',         // Title.
                 'brand_boost_dashboard_widget_display' // Display function.
        );	
}
add_action( 'wp_dashboard_setup', 'brand_boost_add_dashboard_widgets' );

function brand_boost_dashboard_widget_display(){
	if ( ! class_exists('GFAPI' ) ) {
		return;
	}

	$search_criteria = array();
	$sorting         = array();
	$paging          = array( 'offset' => 0, 'page_size' => 9999999 );
	$total_count     = 0;
	$entries         = GFAPI::get_entries( 2, $search_criteria, $sorting, $paging, $total_count );

	$orgs = array();
	$zips = array();

	foreach ( $entries as $e ){
		$orgs[] = $e[2];
		$zips[] = $e[3];
	}

	$orgs = array_count_values($orgs);
	$zips = array_count_values($zips);

	arsort($orgs);
	arsort($zips); // sort by who has the most zip codes

	echo "<h3>Votes</h3><ul>";

	foreach ( $orgs as $key => $value ){
		echo "<li> $key - $value </li>";
	}

	echo "</ul>";

	echo "<h3>Zip Codes (Scroll for More)</h3><ul style='max-height: 200px; overflow: scroll;'>";

	foreach ( $zips as $key => $value ){
		echo "<li> $key - $value </li>";
	}

	echo "</ul>";

}


